This repository contains my Scala code for solving problems for various programming challenges.

Feel free to contact me with any questions regarding my solutions.

I am sharing my code for the problems I have done for the following challenges:
- [Project Euler](https://projecteuler.net/)
- [Advent of Code](http://adventofcode.com/)
- [Hackerrank](https://www.hackerrank.com/faq#sharing-code)

I also have plans to share my code for:
- [Cryptopals](https://www.cryptopals.com/)
- [DailyProgrammer](https://www.reddit.com/r/dailyprogrammer/)

## Running the code
The code examples are implemented as ScalaTest tests,
so that they can be run easily in IntelliJ by
right-clicking on the class name and
choosing `Run ...` or `Debug ...`.


They all print the solution using `println`,
and where I already have the correct solution,
I have added `assert` statements to verify the solution.

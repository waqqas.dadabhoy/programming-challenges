package adventofcode.year2015

import org.scalatest.FunSuite


class puzzle9 extends FunSuite{
  val distances: Seq[String] = """Tristram to AlphaCentauri = 34
Tristram to Snowdin = 100
Tristram to Tambi = 63
Tristram to Faerun = 108
Tristram to Norrath = 111
Tristram to Straylight = 89
Tristram to Arbre = 132
AlphaCentauri to Snowdin = 4
AlphaCentauri to Tambi = 79
AlphaCentauri to Faerun = 44
AlphaCentauri to Norrath = 147
AlphaCentauri to Straylight = 133
AlphaCentauri to Arbre = 74
Snowdin to Tambi = 105
Snowdin to Faerun = 95
Snowdin to Norrath = 48
Snowdin to Straylight = 88
Snowdin to Arbre = 7
Tambi to Faerun = 68
Tambi to Norrath = 134
Tambi to Straylight = 107
Tambi to Arbre = 40
Faerun to Norrath = 11
Faerun to Straylight = 66
Faerun to Arbre = 144
Norrath to Straylight = 115
Norrath to Arbre = 135
Straylight to Arbre = 127""".split("\n")

  def parseLine(s: String): (String, String, Int) = {
    val locationsTogether = s.split(" = ")
    val locationsSeparate = locationsTogether(0).split(" to ")
    (locationsSeparate(0), locationsSeparate(1), locationsTogether(1).toInt)
  }

  def routeDistance(cities: Seq[String]): Int = {
    val citiesFrom = cities.init
    val citiesTo = cities.tail
    (citiesFrom zip citiesTo).map(x => distancesMap((x._1, x._2))).sum
  }

  val distancesMapOneWay: Map[(String, String), Int] = {
    distances.map {line =>
      val parsedLine = parseLine(line)
      (parsedLine._1, parsedLine._2) -> parsedLine._3
    }.toMap
  }

  val distancesMap: Map[(String, String), Int] = {
    distancesMapOneWay ++ distancesMapOneWay.map(x => (x._1._2, x._1._1) -> x._2)
  }

  val locations: Set[String] = distancesMap.keySet.map(_._1)
  val routeDistances: Map[Seq[String], Int] = locations.toSeq.permutations.map(x => x -> routeDistance(x)).toMap

  test("part 1") {
    val answer = routeDistances.values.min
    println(answer)
    assert(answer == 251)
  }

  test("part 2") {
    val answer = routeDistances.values.max
    println(answer)
    assert(answer == 898)
  }

}

package projecteuler

import org.scalatest.FunSuite

import scala.annotation.tailrec

class euler0006 extends FunSuite {
  val sumOfSquares = (1 to 100).map(x => x*x).sum
  val sumOfNumbers = (1 to 100).sum
  val squareOfSum = sumOfNumbers*sumOfNumbers

  test(this.getClass.getName) {
    val answer = squareOfSum - sumOfSquares
    println(answer)
    assert(answer == 25164150)
  }
}

package projecteuler

import org.scalatest.FunSuite

import scala.collection.mutable

class euler0014 extends FunSuite {
  def nextCollatz(n: Long): Long = if (n % 2 == 0) { n/2 } else { 3*n + 1 }

  val chainLengths: mutable.Map[Long, Int] = {
    val result: mutable.Map[Long, Int] = mutable.Map(1L->1, 2L->2, 4L->3, 8L->4, 16L->5, 32L->6)
    (1 to 1000000).foreach { i =>
      if (!result.contains(i)) {
        val numbers: mutable.ArrayBuffer[Long] = mutable.ArrayBuffer.empty
        numbers.append(i)
        while (!result.contains(numbers.last)) {
          numbers.append(nextCollatz(numbers.last))
        }
        for (j <- 0 until numbers.size - 1) {
          result.put(numbers(j), result(numbers.last) + (numbers.size-j-1))
        }
      }
    }
    result
  }

  test(this.getClass.getName) {
    val maxLength: Long = chainLengths.values.max
    val numbersWithLength = chainLengths.filter(x => x._2 == maxLength).keys

    val answer = numbersWithLength.max
    println(answer)
    assert(answer == 837799)
  }
}

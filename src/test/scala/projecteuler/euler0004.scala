package projecteuler

import org.scalatest.FunSuite
import utils.PalindromeUtils

class euler0004 extends FunSuite {

  def getPalindromes(): Long = {
    for (
      i <- 999.to(111, -1);
      j <- 999.to(111, -1)
    ) {
      if (PalindromeUtils.isPalindrome((i*j).toString)) return i*j
    }
    return 0
  }

  test(this.getClass.getName) {
    val allNumbers: Iterator[Int] = 999.to(111, -1).iterator.flatMap { i =>
      i.to(111, -1).iterator.map { j =>
        i*j
      }
    }
    val answer = allNumbers.filter(s => PalindromeUtils.isPalindrome(s.toString)).max
    println(answer)
    assert(answer == 906609)
  }
}

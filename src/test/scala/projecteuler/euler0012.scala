package projecteuler

import org.scalatest.FunSuite
import utils.{DivisionUtils, PrimeUtils, TriangleNumberUtils}

import scala.annotation.tailrec

class euler0012 extends FunSuite {
  @tailrec
  private def loop(minNumDivisors: Int, currentNum: Int = 1): Int = {
    val candidateNumber = TriangleNumberUtils.nth(currentNum)
    val numDivisors = DivisionUtils.numDivisors(candidateNumber)
    if (numDivisors < minNumDivisors) {
      loop(minNumDivisors, currentNum+1)
    } else {
      currentNum
    }
  }

  test(this.getClass.getName) {
    val answer = TriangleNumberUtils.nth(loop(500))
    println(answer)
    assert(answer == 76576500)
  }
}

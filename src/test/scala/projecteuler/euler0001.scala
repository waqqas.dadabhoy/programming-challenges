package projecteuler

import org.scalatest.FunSuite

class euler0001 extends FunSuite {
  test("project euler 1") {
    val answer = (1 until 1000).filter(i => i % 3 == 0 || i % 5 == 0).sum
    println(answer)
    assert(answer == 233168)
  }

}

package projecteuler

import org.scalatest.FunSuite

import scala.annotation.tailrec

class euler0009 extends FunSuite {
  def isPythagoreanTriplet(a: Int, b: Int, c: Int): Boolean = a*a + b*b == c*c

  def x: Int = {
    (1 to 999).foreach { a: Int =>
      (1 to (999 - a)).foreach { b: Int =>
        val c = 1000 - a - b
        if (isPythagoreanTriplet(a, b, c)) return a*b*c
      }
    }
    0
  }

  test(this.getClass.getName) {
    val answer = x
    println(answer)
    assert(answer == 31875000)
  }
}

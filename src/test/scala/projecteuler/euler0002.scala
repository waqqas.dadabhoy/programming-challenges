package projecteuler

import org.scalatest.FunSuite
import utils.FibbonaciUtils

import scala.annotation.tailrec

class euler0002 extends FunSuite {
  test(this.getClass.getName) {
    val answer: Int = FibbonaciUtils.numbersBelow(4000000).filter(_ % 2 == 0).sum
    println(answer)
    assert(answer == 4613732)
  }

}

package projecteuler

import org.scalatest.FunSuite
import utils.PrimeUtils

class euler0007 extends FunSuite {

  test(this.getClass.getName) {
    val answer = PrimeUtils.nthPrime(10001)
    println(answer)
    assert(answer == 104743)
  }
}

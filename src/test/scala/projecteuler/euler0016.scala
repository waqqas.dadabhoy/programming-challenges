package projecteuler

import org.scalatest.FunSuite

import scala.collection.mutable

class euler0016 extends FunSuite {
  test(this.getClass.getName) {
    val num: BigInt = BigInt(2) << 999 // Bitshift is ~50ms faster than `pow`
    val numAsString = num.toString.toCharArray.map(_.toString)

    val answer = numAsString.map(Integer.parseInt).sum
    println(answer)
    assert(answer == 1366)
  }
}

package projecteuler

import org.scalatest.FunSuite
import utils.PalindromeUtils

import scala.annotation.tailrec

class euler0005 extends FunSuite {

  /**
    * Check if a number is divisible by all the numbers between 1 & 20.
    * We only need to check divisiblity by 11 to 20, to cover the full range.
    * This can be further optimized by excluding 12 & 15 from the check.
    * @param n
    * @return true if the number is divisible by all numbers between 1 & 20
    */
  def isEvenlyDividedBy1To20(n: Long): Boolean = {
    Array(11, 13, 14, 16, 17, 18, 19, 20).forall(n % _ == 0)
  }

  @tailrec
  private def getFirstNumberDivisibleBy1To20(n: Long = 20*19*17*13*11): Long = {
    if (isEvenlyDividedBy1To20(n)) {
      n
    } else {
      getFirstNumberDivisibleBy1To20(n+(20*19*17*13*11))
    }
  }

  test(this.getClass.getName) {
    val answer = getFirstNumberDivisibleBy1To20()
    println(answer)
    assert(answer == 232792560)
  }
}

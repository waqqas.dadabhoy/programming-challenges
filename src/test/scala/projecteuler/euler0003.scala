package projecteuler

import org.scalatest.FunSuite
import utils.PrimeUtils

class euler0003 extends FunSuite {

  test(this.getClass.getName) {
    val n: Long = 600851475143L
    val factors: Seq[Long] = PrimeUtils.findPrimeFactors(n) //Primes.primeFactors(600851475143)
    val answer: Long = factors.max
    println(answer)
    assert(answer == 6857)
  }
}

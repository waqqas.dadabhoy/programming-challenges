package projecteuler

import org.scalatest.FunSuite
import utils.PrimeUtils

class euler0010 extends FunSuite {
  test(this.getClass.getName) {
    val answer = PrimeUtils.primesBelow(2000000).sum
    println(answer)
    assert(answer == 142913828922L)
  }
}

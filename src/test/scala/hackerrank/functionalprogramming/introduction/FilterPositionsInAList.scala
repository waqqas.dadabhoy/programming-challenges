package hackerrank.functionalprogramming.introduction

import org.scalatest.FunSuite

class FilterPositionsInAList extends FunSuite {
  def f(arr: List[Int]): List[Int] = {
    val zipped = arr.zipWithIndex
    zipped.filter(_._2 % 2 == 1).map(_._1)
  }


  test("Functional Programming > Introduction > Filter Positions in a List") {
    val l = List(2, 5, 3, 4, 6, 7, 9, 8)
    val answer = f(l)
    println(answer)
    assert(answer == List(5, 4, 7, 8))
  }

}

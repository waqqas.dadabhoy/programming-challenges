package hackerrank.functionalprogramming.introduction

import org.scalatest.FunSuite

class HelloWorldNTimes extends FunSuite {
  def f(n: Int) = (1 to n).foreach(_ => println("Hello World"))

  test("Functional Programming > Introduction > Hello World N Times") {
    f(4)
  }
}

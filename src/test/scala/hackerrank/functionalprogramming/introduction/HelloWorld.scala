package hackerrank.functionalprogramming.introduction

import org.scalatest.FunSuite

class HelloWorld extends FunSuite {
  def f() = println("Hello World")

  test("Functional Programming > Introduction > Hello World") {
    f()
  }
}

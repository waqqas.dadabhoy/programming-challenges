package hackerrank.functionalprogramming.introduction

import org.scalatest.FunSuite

class SumOfOddElements extends FunSuite {
  // Mod of -ve number is -ve, so mods of odd numbers can be 1 or -1
  def f(arr:List[Int]):Int = arr.filter(_ % 2 != 0).sum

  test("Functional Programming > Introduction > Sum of Odd Elements") {
    val l = List(3, 2, 4, 6, 5, 7, 8, 0, 1)
    val answer = f(l)
    println(answer)
    assert(answer == 16)
  }

}

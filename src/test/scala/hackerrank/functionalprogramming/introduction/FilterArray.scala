package hackerrank.functionalprogramming.introduction

import org.scalatest.FunSuite

class FilterArray extends FunSuite {

  def f(delim: Int, arr: List[Int]): List[Int] = {
    // Not using filter as recommended in problem description
    arr.flatMap(i => if (i < delim) Option(i) else None)
  }


  test("Functional Programming > Introduction > Filter Array") {
    val l = List(10, 9, 8, 2, 7, 5, 1, 3, 0)
    val answer = f(3, l)
    println(answer)
    assert(answer == List(2, 1, 0))
  }

}

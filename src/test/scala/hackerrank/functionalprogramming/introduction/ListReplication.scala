package hackerrank.functionalprogramming.introduction

import org.scalatest.FunSuite

class ListReplication extends FunSuite {
  def f(num: Int, arr: List[Int]): List[Int] = {
    def repeatItem(num: Int, item: Int): List[Int] = List.fill(num)(item)

    arr.flatMap(i => repeatItem(num, i))
  }

  test("Functional Programming > Introduction > List Replication") {
    val answer = f(3, List(1,2,3,4))
    println(answer)
    assert(answer == List(1,1,1,2,2,2,3,3,3,4,4,4))
  }
}

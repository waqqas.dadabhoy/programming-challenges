package hackerrank.functionalprogramming.introduction

import org.scalatest.FunSuite

class ListLength extends FunSuite {
  def f(arr:List[Int]):Int = {
    def f_inner(arr: List[Int], currentSize: Int): Int = {
      if (arr.isEmpty) {
        currentSize
      } else {
        f_inner(arr.tail, currentSize+1)
      }
    }

    f_inner(arr, 0)
  }


  test("Functional Programming > Introduction > List Length") {
    val l = List(2, 5, 1, 4, 3, 7, 8, 6, 0, 9)
    val answer = f(l)
    println(answer)
    assert(answer == 10)
  }

}

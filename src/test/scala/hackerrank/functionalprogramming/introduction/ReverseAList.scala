package hackerrank.functionalprogramming.introduction

import org.scalatest.FunSuite

class ReverseAList extends FunSuite {

  def f(arr: List[Int]): List[Int] = {
    // Not using reverse as recommended in problem description
    if (arr.isEmpty || arr.lengthCompare(1) == 0) {
      arr
    } else {
      arr.last :: f(arr.init)
    }
  }


  test("Functional Programming > Introduction > Reverse a List") {
    val l = List(19, 22, 3, 28, 26, 17, 18, 4, 28, 0)
    val answer = f(l)
    println(answer)
    assert(answer == List(0, 28, 4, 18, 17, 26, 28, 3, 22, 19))
  }

}

package hackerrank.functionalprogramming.recursion

import org.scalatest.FunSuite

class ComputingTheGcd extends FunSuite {
  def gcd(x: Int, y: Int): Int = {
    if (x == y) {
      x
    } else if (x > y) {
      gcd(x-y, y)
    } else {
      gcd(x, y-x)
    }
  }

  test("Functional Programming > Recursion > Computing the GCD") {
    assert(gcd(1, 5) == 1)
    assert(gcd(10, 100) == 10)
    assert(gcd(22, 131) == 1)
  }
}

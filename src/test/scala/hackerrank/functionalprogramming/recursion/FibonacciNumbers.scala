package hackerrank.functionalprogramming.recursion

import org.scalatest.FunSuite

class FibonacciNumbers extends FunSuite {
  final def fibonacciSeq(n: Int, prevSeq: List[Int] = List(1, 0)): List[Int] = {
    if (prevSeq.lengthCompare(n) >= 0) {
      prevSeq
    } else {
      val nextNumber = prevSeq(0) + prevSeq(1)
      fibonacciSeq(n, nextNumber :: prevSeq)
    }
  }

  def fibonacci(x:Int):Int = fibonacciSeq(x).head

  test("Functional Programming > Recursion > Computing the GCD") {
    assert(fibonacci(3) == 1)
    assert(fibonacci(4) == 2)
    assert(fibonacci(5) == 3)
    assert(fibonacci(30) == 514229)

  }
}

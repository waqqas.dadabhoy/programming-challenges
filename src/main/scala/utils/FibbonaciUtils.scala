package utils

import scala.annotation.tailrec

object FibbonaciUtils {
  @tailrec
  final def numbersBelow(n: Int, prevSeq: List[Int] = List(2, 1)): List[Int] = {
    val nextNumber = prevSeq(0) + prevSeq(1)
    if (nextNumber >= n) {
      prevSeq
    } else {
      numbersBelow(n, nextNumber :: prevSeq)
    }
  }
}
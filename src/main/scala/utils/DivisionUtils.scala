package utils

import scala.collection.mutable

object DivisionUtils {
  def getDivisors(n: Int): Seq[Int] = {
    (1 to (n/2)).filter(x => n % x == 0).flatMap(x => Seq(x, n/x)).distinct
  }

  def factorise(n: Long): mutable.Map[Long, Int] = n match {
    case 1 => mutable.Map.empty
    case n => {
      val result: mutable.Map[Long, Int] = mutable.Map[Long, Int]().withDefaultValue(0)
      val primesToTryUpTo = Math.sqrt(n).ceil.toLong + 1
      val primesToTry: Seq[Long] = PrimeUtils.primesBelow(primesToTryUpTo)

      var afterDivision = n
      primesToTry.foreach { p: Long =>
        var numberOfTimesDivided = 0
        while (afterDivision % p == 0) {
          numberOfTimesDivided += 1
          afterDivision = afterDivision / p
        }
        result.update(p, numberOfTimesDivided)
      }
      if (afterDivision > 1) { result.update(afterDivision, 1) }
      result
    }
  }

  def numDivisors(n: Long): Int = {
    val factors = factorise(n)
    factors.values.map(_ + 1).product
  }
}

package utils

import scala.annotation.tailrec

object PalindromeUtils {
  @tailrec
  def isPalindrome(s: String): Boolean = {
    if (s.isEmpty || s.length == 1) {
      true
    } else {
      if (s.head == s.last) {
        isPalindrome(s.tail.init)
      } else {
        false
      }
    }
  }

}

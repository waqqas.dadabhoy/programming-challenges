package hackerrank.weekofcode23.gearsofwar

import scala.collection.mutable
object Solution {

  def main(args: Array[String]) {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution
*/
    val n: Int = scala.io.StdIn.readLine().toInt
    val answers: mutable.ArraySeq[String] = new mutable.ArraySeq[String](n)
    for (i <- 0 until n) {
      val num_gears: Int = scala.io.StdIn.readLine().toInt
      answers(i) = if (num_gears % 2 == 0) "Yes" else "No"
    }
    for (a <- answers) {
      println(a)
    }
  }
}

package hackerrank.weekofcode23.lighthouse

object Solution {

  case class Point(val x: Int, val y: Int) {
    override def toString: String = "Point(" + this.x + "," + this.y + ")"
  }

  def main(args: Array[String]): Unit = {
    val n: Int = scala.io.StdIn.readLine().toInt
    val landscape: scala.collection.mutable.Map[Point, Char] = scala.collection.mutable.Map.empty
    for (i <- 0 until n) {
      val line: Seq[Char] = scala.io.StdIn.readLine().toList
      for (j <- 0 until n) {
        landscape(Point(j, i)) = line(j)
      }
    }

    var largest_radius: Int = 0
    for ((position, c) <- landscape) {
      val current_radius: Int = getLargestRadiusForPoint(position, landscape)
      if (current_radius > largest_radius) { largest_radius = current_radius}
    }
    println(largest_radius)

  }

  def getLargestRadiusForPoint(center: Point, landscape: scala.collection.mutable.Map[Point, Char]): Int = {
    if (landscape.getOrElse(center, '*') == '*') return 0
    for (i <- 0 to 50) {
      val circle_points = getAllPointsForCircle(center, i)
      val circle_landscape = circle_points.map(landscape.getOrElse(_, '*'))
      if (circle_landscape.contains('*')) return i-1
    }
    throw new Exception("Maximum dimension of circle reached, for " + center.toString)
  }

  def getAllPointsForCircle(center: Point, radius: Int): Seq[Point] = {
    val allPoints = for (
      i <- (center.x-radius) to (center.x+radius);
      j <- (center.y-radius) to (center.y+radius)
    ) yield Point(i,j)

    val filtered_points = allPoints.filter(isPointInsideCircle(_, center, radius))
    filtered_points
  }

  def isPointInsideCircle(point: Point, center: Point, radius: Int): Boolean = {
    val result = math.pow(point.x - center.x, 2) + math.pow(point.y - center.y, 2) <= math.pow(radius, 2)
    result
  }
}



package hackerrank.euler.euler001

object Solution {
  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    var t = sc.nextInt();
    var a0 = 0;
    while(a0 < t){
      var n: Int = sc.nextInt();
      a0+=1;

      val multiplesOf3: Long = 3L.until(n, 3L).sum
      val multiplesOf5: Long = 5L.until(n, 5L).sum
      val multiplesOf15: Long = 15L.until(n, 15L).sum

      val answer: Long = multiplesOf3 + multiplesOf5 - multiplesOf15
      println(answer)
    }
  }

}

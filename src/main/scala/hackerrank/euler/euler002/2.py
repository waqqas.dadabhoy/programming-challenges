import math

fibbonaci = [1, 2, 3, 5, 8]
fibbonaci_even = []

i = fibbonaci[-1]
while (i < 4 * 10**16):
    fibbonaci.append(fibbonaci[-1] + fibbonaci[-2])
    i = fibbonaci[-1]

for x in fibbonaci:
    if x % 2 == 0:
        fibbonaci_even.append(x)

t = int(input())

for i in range(t):
    n = int(input())
    sum = 0
    for x in fibbonaci_even:
        if x < n:
            sum += x
    print(sum)

#print(fibbonaci)
#print(fibbonaci_even)
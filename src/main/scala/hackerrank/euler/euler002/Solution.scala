package hackerrank.euler.euler002

import scala.annotation.tailrec

object Solution {
  val fibsEven: List[Long] = buildFibs(2, 1, List(2))

  @tailrec
  def buildFibs(lastTerm: Long, secondLastTerm: Long, l: List[Long]): List[Long] = {
    if (l.head > 4*Math.pow(10, 16)) {
      l
    } else {
      val newLastTerm = lastTerm + secondLastTerm
      buildFibs(newLastTerm, lastTerm, if (newLastTerm % 2 == 0) newLastTerm :: l else l)
    }
  }

  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    var t = sc.nextInt();
    var a0 = 0;
    while(a0 < t){
      var n: Long = sc.nextLong();
      a0+=1;

      val answer: Long = fibsEven.filter(_ <= n).sum
      println(answer)
    }
  }

}

package hackerrank.functionalprogramming.introduction.evaluatingex

object Solution {
  def factorial(n: Int): Int = n match {
    case 0 => 1
    case n: Int => (1 to n).product
  }

  def ex(x: Double): Double = {
    (0 until 10).map(i => Math.pow(x, i)/factorial(i)).sum
  }

  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    var n = sc.nextInt();
    var a0 = 0;
    while(a0 < n){
      var x = sc.nextDouble();
      a0+=1;

      println(ex(x))
    }
  }
}


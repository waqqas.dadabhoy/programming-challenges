package hackerrank.functionalprogramming.recursion.pascalstriangle

object Solution {
  def factorial(n: Int): Int = (1 to n).product

  def pascalRow(n: Int): Seq[Int] = {
    (0 to n).map(r => factorial(n)/(factorial(r) * factorial(n-r)))
  }

  def main(args: Array[String]): Unit = {
    val n = io.Source.stdin.getLines().take(1).toSeq.head.toInt
    (0 until n).foreach { i =>
      println(pascalRow(i).mkString(" "))
    }
  }

}

package hackerrank.functionalprogramming.recursion.stringmingling

class Solution {
  def main(args: Array[String]): Unit = {
    val strings: Seq[String] = io.Source.stdin.getLines().take(2).toSeq
    val p: String = strings.head
    val q: String = strings.last

    println((0 until p.length + q.length).map(i => if (i%2 == 0) p(i/2) else q((i-1)/2)).mkString)
  }

}

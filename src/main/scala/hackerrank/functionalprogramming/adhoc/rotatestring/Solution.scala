package hackerrank.functionalprogramming.adhoc.rotatestring

object Solution {
  def rotateLeft(s: String): String = {
    s.tail + s.head
  }

  def printRotations(s: String): Unit = {
    val rotations = (0 until s.length).scanLeft(s)((x, y) => rotateLeft(x)).tail
    println(rotations.mkString(" "))
  }


  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    var t = sc.nextLine().toInt;
    var a0 = 0;
    while(a0 < t) {
      var s: String = sc.nextLine()
      a0 += 1

      printRotations(s)
    }
  }
}

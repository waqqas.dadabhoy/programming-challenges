package hackerrank.algorithms.warmup.minimaxsum

object Solution {

  def miniMaxSum(arr: Array[Int]): Unit =  {
    assert(arr.length == 5)
    val sortedArr: Seq[Long] = arr.toSeq.map(_.toLong).sorted
    val min = sortedArr.init.sum
    val max = sortedArr.tail.sum
    println(min + " " + max)
  }

  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    var arr = new Array[Int](5);
    for(arr_i <- 0 to 5-1) {
      arr(arr_i) = sc.nextInt();
    }
    miniMaxSum(arr);
  }
}

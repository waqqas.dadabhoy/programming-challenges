package hackerrank.algorithms.warmup.solvemefirst

object Solution {
  // This code is provided by HackerRank
  def main(args: Array[String]) {
    println(io.Source.stdin.getLines().take(2).map(_.toInt).sum)
  }
}

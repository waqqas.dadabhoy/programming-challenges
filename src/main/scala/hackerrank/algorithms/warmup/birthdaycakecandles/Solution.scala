package hackerrank.algorithms.warmup.birthdaycakecandles

object Solution {

  def birthdayCakeCandles(n: Int, ar: Array[Int]): Int =  {
    val counts: Map[Int, Int] = ar.groupBy(identity).mapValues(_.length)
    counts.maxBy(_._1)._2
  }

  def main(args: Array[String]) {
    val sc = new java.util.Scanner (System.in);
    var n = sc.nextInt();
    var ar = new Array[Int](n);
    for(ar_i <- 0 to n-1) {
      ar(ar_i) = sc.nextInt();
    }
    val result = birthdayCakeCandles(n, ar);
    println(result)
  }
}
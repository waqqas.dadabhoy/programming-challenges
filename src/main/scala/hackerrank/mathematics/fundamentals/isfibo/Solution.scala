package hackerrank.mathematics.fundamentals.isfibo

import scala.collection.mutable

object Solution {
  val fibs: mutable.Buffer[Long] = mutable.Buffer(0, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144)

  def main(args: Array[String]): Unit = {
    val sc = new java.util.Scanner (System.in);
    var t = sc.nextInt();
    var a0 = 0;
    while(a0 < t) {
      var n: Long = sc.nextLong()
      a0 += 1

      while (fibs.last < n) {
        fibs.append(fibs.last + fibs.init.last)
      }
      if (fibs.contains(n)) {
        println("IsFibo")
      } else {
        println("IsNotFibo")
      }
    }
  }

}

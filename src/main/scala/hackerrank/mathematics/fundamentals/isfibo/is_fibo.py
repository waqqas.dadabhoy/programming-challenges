fibs = [0,1,2, 3, 5, 8,13 , 21 , 34 , 55 , 89 , 144]

t = int(input())
for i in range(t):
    n = int(input())
    while fibs[-1] < n:
        fibs.append(fibs[-1] + fibs[-2])
    if n in fibs:
        print("IsFibo")
    else:
        print("IsNotFibo")

scalaVersion := "2.12.4"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.7.+"
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.8.7"
libraryDependencies += "commons-codec" % "commons-codec" % "1.11"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.4"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"

testOptions in Test += Tests.Argument("-oD")
